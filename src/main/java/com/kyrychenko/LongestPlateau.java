package com.kyrychenko;

public class LongestPlateau {
    private int maxPos = 0;
    private int maxLength = 0;
    private int[] data;

    public LongestPlateau(int[] data) {
        this.data = data;
    }

    public void findLongestPlateau() {
        int currentPos = 0, currentLength = 0;
        int lastVal = data[0];

        for (int i = 1; i < data.length; i++) {
            if (data[i] > lastVal) {
                currentPos = i;
                currentLength = 1;
            } else if (data[i] == lastVal && currentLength > 0) {
                currentLength++;
            } else if (data[i] < lastVal && currentLength > 0) {
                if (currentLength > maxLength) {
                    maxLength = currentLength;
                    maxPos = currentPos;
                }

                currentLength = 0;
                currentPos = 0;
            }

            lastVal = data[i];
        }
    }

    public int getMaxPos() {
        return maxPos;
    }

    public int getMaxLength() {
        return maxLength;
    }
}
