package com.kyrychenko;

public class Minesweeper {
    private int m;
    private int n;
    private double p;
    private boolean[][] game;
    private int[][] sol;
    private MinesweeperGameGenerator generator;

    public Minesweeper(int m, int n, double p, MinesweeperGameGenerator generator) {
        this.m = m;
        this.n = n;
        this.p = p;
        this.generator = generator;
    }

    public void gameSolution() {
        this.game = this.generator.initGame(m, n, p);
        sol = new int[m + 2][n + 2];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                for (int k = i - 1; k <= i + 1; k++) {
                    for (int l = j - 1; l <= j + 1; l++) {
                        if (game[k][l]) sol[i][j]++;
                    }
                }
            }
        }
    }

    public void printGame() {
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++)
                if (game[i][j]) System.out.print("* ");
                else System.out.print(". ");
            System.out.println();
        }
    }

    public void printSolution() {
        System.out.println();
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++)
                if (game[i][j]) {
                    System.out.print("* ");
                } else System.out.print(sol[i][j] + " ");
            System.out.println();
        }
    }

    public boolean[][] getGame() {
        return game;
    }

    public int[][] getSol() {
        return sol;
    }
}
