package com.kyrychenko;

public class MinesweeperGameGenerator {
    public boolean[][] initGame(int m, int n, double p) {
        boolean[][] game = new boolean[m + 2][n + 2];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                game[i][j] = (Math.random() < p);
            }
        }
        return game;
    }
}
