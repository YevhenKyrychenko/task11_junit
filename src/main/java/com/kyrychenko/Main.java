package com.kyrychenko;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Main {
    private  static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] data = new int[n];

        for (int i = 0; i < n; i++)
            data[i] = (int) (Math.random() * 10);

        LongestPlateau plateau = new LongestPlateau(data);
        plateau.findLongestPlateau();
        logger.info("length" + plateau.getMaxLength());
        logger.info("position" + plateau.getMaxPos());

        Minesweeper minesweeper = new Minesweeper(5, 5, 0.3, new MinesweeperGameGenerator());
        minesweeper.gameSolution();
        minesweeper.printGame();
        minesweeper.printSolution();
    }
}
