package com.kyrychenko;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class MinesweeperTest {
    private static final int M = 3;
    private static final int N = 3;
    private static final double P = 0.3;
    private Minesweeper minesweeper;

    private MinesweeperGameGenerator generator;

    @BeforeEach
    void setUp() {
        generator = Mockito.mock(MinesweeperGameGenerator.class);
        minesweeper = new Minesweeper(M, N, P, generator);
    }

    @Test
    void gameSolution() {
        Mockito.when(generator.initGame(M, N, P)).thenReturn(new boolean[][] {
                {false, false, false, false, false},
                {false, true, false, false, false},
                {false, false, false, true, false},
                {false, true, false, false, false},
                {false, false, false, false, false}
        });
        minesweeper.gameSolution();
        assertArrayEquals(new int[][]{
                {0, 0, 0, 0, 0},
                {0, 1, 2, 1, 0},
                {0, 2, 3, 1, 0},
                {0, 1, 2, 1, 0},
                {0, 0, 0, 0, 0}
        }, minesweeper.getSol());
        Mockito.verify(generator, Mockito.times(1)).initGame(M, N, P);
    }
}