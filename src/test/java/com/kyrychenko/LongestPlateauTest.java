package com.kyrychenko;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@RunWith(Parameterized.class)
class LongestPlateauTest {

    @ParameterizedTest
    @MethodSource("getTestData")
    void findLongestPlateau(int[] testData, int testMaxPos, int testMaxLength) {
        LongestPlateau plateau = new LongestPlateau(testData);
        plateau.findLongestPlateau();
        assertEquals(testMaxLength, plateau.getMaxLength());
        assertEquals( testMaxPos, plateau.getMaxPos());
    }

    static Stream<Arguments> getTestData() {
        return Stream.of(
                arguments(new int[]{1, 2, 2, 2, 1}, 1, 3),
                arguments(new int[]{1, 2, 2, 2, 3}, 0, 0),
                arguments(new int[]{3, 2, 2, 2, 1}, 0, 0),
                arguments(new int[]{3, 2, 2, 2, 3}, 0, 0)
        );
    }
}