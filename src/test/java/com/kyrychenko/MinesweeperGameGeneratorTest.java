package com.kyrychenko;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinesweeperGameGeneratorTest {
    private static final int M = 3;
    private static final int N = 3;
    private static final double P = 0.3;
    private MinesweeperGameGenerator generator;

    @BeforeEach
    void setUp() {
        this.generator = new MinesweeperGameGenerator();
    }

    @Test
    void initGame() {
        assertNotNull(generator.initGame(M, N, P));
    }
}